from tqdm import tqdm
import string
import math
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.metrics.pairwise import cosine_similarity

def downloadPackage():
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    nltk.download('stopwords')

def textPreprocess(text):
    not_need_char_arr = ['\r', '\n', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '©', '«', '»', '•','—']
    for ch in not_need_char_arr:
        text = text.replace(ch, ' ')
    for pun in string.punctuation:
        text = text.replace(pun, ' ')
    return text

def textLemmatizer(text):
    lm = WordNetLemmatizer()
    text = lm.lemmatize(text)
    return text

def judgeStopWords(word):
    if word in stopwords.words('english'):
        return True
    else:
        return False

def createInvertedIndexDictionary():
    print("開始建立InvertedIndex字典")
    data = {}
    total_doc_num = 0
    with open('output.txt', 'r', encoding='utf-8') as f:
        word_info_arr = f.read().split('\n\n')
        for word_info in tqdm(word_info_arr):
            word_arr = word_info.split('\n')
            word = word_arr[0].split(',')[0]
            doc_index_arr = word_arr[1:]
            doc_word_count = {}
            for doc in doc_index_arr:
                doc_split = doc.split(',')
                doc = doc_split[0].replace('<', '').replace(' ','')
                doc_word_count[doc] = doc_split[1].split(':')[0]
                if int(doc) > total_doc_num:
                    total_doc_num = int(doc)
            data[word]= doc_word_count
    print("建立完成")
    return data, total_doc_num

def createDocumentsTFIDF(terms_arr, word_dic, doc_number, total_doc_num):
    tfidf_list = []
    for word in terms_arr:
        if judgeStopWords(word) is False:
            if word in word_dic.keys():
                if doc_number in word_dic[word].keys():
                    tf = 1 + math.log10(int(word_dic[word][doc_number])) 
                else:
                    tf = 0
                idf = math.log10(total_doc_num / len(word_dic[word]))
                w = tf * idf
            else:
                w = 0
            tfidf_list.append(w)
    return tfidf_list

def createNormalModeDocumentsTFIDFList(terms_arr, word_dic, total_doc_num):
    print("開始建立文章TFIDF串列")
    data = []
    for doc_number in tqdm(range(1, total_doc_num + 1)):
        tfidf_list = createDocumentsTFIDF(terms_arr, word_dic , str(doc_number), total_doc_num)
        data.append(tfidf_list)
    print("建立完成")
    return data

def judgeAllWordInDoc(terms_arr, word_dic, doc_number):
    exist = 0
    for word in terms_arr:
        if word in word_dic.keys() and doc_number in word_dic[word].keys():
            exist += 1
    if exist == len(terms_arr):
        return True
    else:
        return False

def createAndModeDocumentsTFIDFList(terms_arr, word_dic, total_doc_num):
    print("開始建立文章TFIDF串列")
    data = []
    for doc_number in tqdm(range(1, total_doc_num + 1)):
        if judgeAllWordInDoc(terms_arr, word_dic, str(doc_number)):
            tfidf_list = createDocumentsTFIDF(terms_arr, word_dic , str(doc_number), total_doc_num)
        else:
            tfidf_list = [0] * len(terms_arr)
        data.append(tfidf_list)
    print("建立完成")
    return data

def createOrModeDocumentsTFIDFList(terms_arr, word_dic, total_doc_num):
    print("開始建立文章TFIDF串列")
    data = []
    for doc_number in tqdm(range(1, total_doc_num + 1)):
        tfidf_list = []
        for terms in terms_arr:
            if judgeAllWordInDoc(terms, word_dic, str(doc_number)):
                tfidf_list += createDocumentsTFIDF(terms, word_dic , str(doc_number), total_doc_num)
            else:
                tfidf_list += [0] * len(terms)
        data.append(tfidf_list)
    print("建立完成")
    return data

def createQueryTFIDFList(query_dict, word_dic, total_doc_num):
    print("開始建立queryTFIDF串列")
    data = []
    for (word, count) in tqdm(query_dict.items()):
        if judgeStopWords(word) is False:
            if word in word_dic.keys():
                tf = 1 + math.log10(count)
                idf = math.log10(total_doc_num / len(word_dic[word]))
                w = tf * idf
            else:
                w = 0
            data.append(w)
    print("建立完成")
    return data

def cosineSimilarityScore(query_dict, terms_arr, mode): 
    word_dic, total_doc_num = createInvertedIndexDictionary()
    if mode == 'AND':
        doc_tfidf_arr = createAndModeDocumentsTFIDFList(terms_arr, word_dic, total_doc_num)
    elif mode == 'OR':
        doc_tfidf_arr = createOrModeDocumentsTFIDFList(terms_arr, word_dic, total_doc_num)
    else:
        doc_tfidf_arr = createNormalModeDocumentsTFIDFList(terms_arr, word_dic, total_doc_num)
    query_tfidf = createQueryTFIDFList(query_dict, word_dic, total_doc_num)
    cosine_similarity_score = {}
    print("開始比較CosineSimilarity")
    for doc_index in tqdm(range(0, len(doc_tfidf_arr))):
        cosine_similarity_score[doc_index + 1] = round(cosine_similarity([query_tfidf], [doc_tfidf_arr[doc_index]])[0, 0], 3)
    data = {}
    while (len(cosine_similarity_score) != 0):
       max_key = max(cosine_similarity_score, key = cosine_similarity_score.get)
       data[max_key] = cosine_similarity_score.pop(max_key)
    print("比較完成")
    return data

def queryPreprocess(text):
    text = text.lower()
    text = textPreprocess(text)
    query = text.split()
    query_dict = {}
    for word in query:
        lm_word = textLemmatizer(word)
        if lm_word not in query_dict:
            query_dict[lm_word] = 1
        else:
            query_dict[lm_word] += 1
    return query_dict

def orTermsPreprocess(text):
    terms_arr = text.split('OR')
    for terms_index in range(0, len(terms_arr)):
        terms_arr[terms_index] = queryPreprocess(terms_arr[terms_index]).keys()
    return terms_arr

def andTermsPreprocess(text):
    text = text.replace('AND', ' ')
    terms_arr = queryPreprocess(text).keys()
    return terms_arr

def normalTermsPreprocess(text):
    terms_arr = queryPreprocess(text).keys()
    return terms_arr

def findCosineSimilarity(text):
    downloadPackage()
    mode = 'Normal'
    if 'AND' in text:
        mode = 'AND'
        terms_arr = andTermsPreprocess(text)
    elif 'OR' in text:
        mode = 'OR'
        terms_arr = orTermsPreprocess(text)
    else:
        terms_arr = normalTermsPreprocess(text)
    if len(terms_arr) == 0:
        print("請勿輸入空白查詢")
    else:
        query_dict = queryPreprocess(text)
        result = cosineSimilarityScore(query_dict, terms_arr, mode)
        print("開始生成檔案")
        f = open('score.txt', 'w')
        for (key, value) in result.items():
            f.write(f"{str(key):8}{value}\n")
        f.close()
        print("檔案生成完成")

if __name__ == '__main__':
    text = input("輸入要搜尋的字: ")
    findCosineSimilarity(text)